output "network_name" {
  value = module.network.network_name
}

output "network_id" {
  value = module.network.network_id
}

output "subnetwork_id" {
  value = module.subnetwork.subnetwork_id
}

output "subnetwork_cidr" {
  value = module.subnetwork.subnetwork_v4_cidr_blocks
}

output "subnetwork_zone" {
  value = module.subnetwork.subnetwork_zone
}

output "vm_game01_hostname" {
  value = module.vm-game01.hostname
}

output "vm_game01_internal_ip" {
  value = module.vm-game01.internal_ip
}

output "vm_game01_external_ip" {
  value = module.vm-game01.external_ip
}

output "vm_game02_hostname" {
  value = module.vm-game02.hostname
}

output "vm_game02_internal_ip" {
  value = module.vm-game02.internal_ip
}

output "vm_game02_external_ip" {
  value = module.vm-game02.external_ip
}

output "vm_game03_hostname" {
  value = module.vm-game03.hostname
}

output "vm_game03_internal_ip" {
  value = module.vm-game03.internal_ip
}

output "vm_game03_external_ip" {
  value = module.vm-game03.external_ip
}

output "vm_haproxy_hostname" {
  value = module.vm-haproxy.hostname
}

output "vm_haproxy_internal_ip" {
  value = module.vm-haproxy.internal_ip
}

output "vm_haproxy_external_ip" {
  value = module.vm-haproxy.external_ip
}